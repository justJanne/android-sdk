FROM openjdk:8-jdk AS builder

# define arguments for versions to be used for platform and build tools
ARG PLATFORM=35
ARG BUILD_TOOLS=35.0.0

RUN apt-get --quiet update --yes && \
    apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip && \
    unzip -q android-sdk.zip -d android-sdk-linux && \
    rm android-sdk.zip
RUN mkdir android-sdk-linux/licenses && \
    printf "8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e\n24333f8a63b6825ea9c5514f83c2829b004d1fee" > android-sdk-linux/licenses/android-sdk-license && \
    printf "84831b9409646a918e30573bab4c9c91346d8abd" > android-sdk-linux/licenses/android-sdk-preview-license
RUN android-sdk-linux/tools/bin/sdkmanager --update > /dev/null
RUN android-sdk-linux/tools/bin/sdkmanager \
        "platforms;android-${PLATFORM}" \
        "build-tools;${BUILD_TOOLS}" \
        "extras;google;m2repository" \
        "extras;android;m2repository" > /dev/null

FROM openjdk:17-jdk-bullseye
COPY --from=builder /android-sdk-linux /android-sdk-linux
ENV PATH=$PATH:$ANDROID_HOME/platform-tools/
ENV ANDROID_SDK_ROOT=$PWD/android-sdk-linux
ENV PLATFORM=$PLATFORM
ENV BUILD_TOOLS=$BUILD_TOOLS
ENV GRADLE_OPTS="-Dorg.gradle.daemon=False"
